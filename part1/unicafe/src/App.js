import React, { useState } from 'react'






const Buttons = (props) => {
  return (
    <>
      <button onClick={props.fns[0]} style={{margin: "0px 10px"}}>
        Good
      </button>
      <button onClick={props.fns[1]} style={{margin: "0px 10px"}}>
        Neutral
      </button>
      <button onClick={props.fns[2]} style={{margin: "0px 10px"}}>
        Bad
      </button>
    </>
  )
}

const Stats = (props) => {
  let totalRevs = props.stats[0] + props.stats[1] + props.stats[2]
  var weightedRate = (((props.stats[0] + props.stats[1]/1.5) / (totalRevs)) * 100).toFixed(2)

  if (totalRevs) {
    return (
      <>
      <p>
        Good: {props.stats[0]} <br/> Neutral: {props.stats[1]} <br/> Bad: {props.stats[2]} <br /> 
        Rating: {weightedRate}{(weightedRate = 0) ? "" : "%"}
      </p>
    </>
    )
  } else {
    return (
      <>
      <p>
        No Feedback Submitted.
      </p>
    </>
    )
  }
}

const Anecdotes = (props) => {

  return (
    <div style={{border: "2px solid", maxWidth: "fit-content", padding: "5px"}}>
      <p style={{maxHeight: "35px", minHeight: "35px"}} id="anecP">
        {props.anec}
      </p>
      <button onClick={props.clickHandler}>
        click me
      </button>
      <button onClick={props.voteUp}>
        vote up
      </button>
      <button onClick={props.voteDown}>
        vote down
      </button>
      <p>
        Score: {props.score}
      </p>
    </div>
  )
}

const TopAnecdote = (props) => {
  
  let style={
    maxWidth: "60%",
    minWidth: "60%",
    textAlign: "center",
    marginRight: "100px",
    position: "absolute",
    top: "15%",
    right: 0,
    border: "2px solid"
  }
  
  if (props.anecdote === "") {
    return (
      <>
      </>
    )
  } else {
    return (
      <div style={style}>
        <h1>
          Top Anecdote
        </h1>
        <p>{props.anecdote}</p>
      </div>
    )
  }
}

const App = () => {
  // save clicks of each button to its own state
  const [good, setGood] = useState(0)
  const [neutral, setNeutral] = useState(0)
  const [bad, setBad] = useState(0)
  const [ranking, setRanking] = useState({
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
  })
  const [anecID, setAnecID] = useState(0)
  const [topAnecdote, setTopAnecdote] = useState("")

  const handleGood = () => {
    setGood(good + 1);
  }
  const handleBad = () => {
    setBad(bad + 1);
  }
  const handleNeutral = () => {
    setNeutral(neutral + 1);
  }

  let feedBackFunctions = [handleGood, handleNeutral, handleBad]

  const anecdotes = [
    'If it hurts, do it more often',
    'Adding manpower to a late software project makes it later!',
    'The first 90 percent of the code accounts for the first 10 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
    'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
    'Premature optimization is the root of all evil.',
   'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.',
    'Programming without an extremely heavy use of console.log is same as if a doctor would refuse to use x-rays or blood tests when diagnosing patients',
    "",
  ]

  const anecButtonHandler = () => {
    
    setAnecID(() => Math.floor(Math.random() * 7))

  }

  const voteUpHandler = () => {
    let newRanking = {...ranking}
    newRanking[anecID] = ranking[anecID] + 1
    setRanking(newRanking)
  }

  const voteDownHandler = () => {
    let newRanking = {...ranking}
    newRanking[anecID] = ranking[anecID] - 1
    setRanking(newRanking)
  }

  let checkMax = () => {
    let scoreArray = []
    Object.values(ranking).forEach(val => {
        scoreArray.push(val)
    })
    let topScore = Math.max(...scoreArray)
    let index

    for (let i = 0; i < Object.keys(ranking).length; i++) {
      if (ranking[i] === topScore) {
        index = i
        break;
      }
    }

    if (topScore === 0) {
    } else {
      if (anecdotes[index] == topAnecdote) {
      } else {
        setTopAnecdote(anecdotes[index])
      }
    }
  }

  checkMax()

  return (
    <div>
      <h1>
        Give Feedback:
      </h1>
      <Buttons fns={feedBackFunctions} />
      <br />
      <br />
      <div style={{border: "2px solid", maxWidth: "fit-content"}}>
      <h2>
        Statistics...
      </h2>
      <Stats stats={[good, neutral, bad]} />
      </div>
      <br />
      <TopAnecdote anecdote={topAnecdote} />
      <Anecdotes clickHandler={anecButtonHandler} voteUp={voteUpHandler} voteDown={voteDownHandler} anec={anecdotes[anecID]} score={ranking[anecID]} />
    </div>
  )
}

export default App