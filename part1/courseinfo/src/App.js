import React from 'react'

const Header = (props) => {
  return (
    <>
      <h1>
        {props.course.name}
      </h1>
    </>
  )
}

const Part1 = (props) => {
  return (
    <>
      <p>
        {props.course.parts[0].name}
      </p>
      <ul>
        <li>
          {props.course.parts[0].numExercises} Exercises
        </li>
      </ul>
    </>
  )
}

const Part2 = (props) => {
  return (
    <>
      <p>
        {props.course.parts[1].name}
      </p>
      <ul>
        <li>
          {props.course.parts[1].numExercises} Exercises
        </li>
      </ul>
    </>
  )
}

const Part3 = (props) => {
  return (
    <>
      <p>
        {props.course.parts[2].name}
      </p>
      <ul>
        <li>
          {props.course.parts[2].numExercises} Exercises
        </li>
      </ul>
    </>
  )
}

const Content = (props) => {
  return (
    <>
      <Part1 course={props.course} course={props.course} />
      <Part2 course={props.course} course={props.course} />
      <Part3 course={props.course} course={props.course} />
    </>
  )
}

const Total = (props) => {
  return (
    <>
      <p>
        There are a total of {props.course.parts[0].numExercises + props.course.parts[1].numExercises + props.course.parts[2].numExercises} exercises!
      </p>
    </>
  )
}
const App = () => {


  const course = {
    name: 'Half Stack Application Development',
    parts: [
      {
        name: 'Fundamentals of React',
        numExercises: 10
      },
      {
        name: 'Using props to pass data',
        numExercises: 7
      },
      {
        name: 'State of a component',
        numExercises: 14
      }
    ]

  }


  return (
    <>
      <Header course={course} />
      <Content course={course} />
      <Total course={course} />
    </>
  )
}

export default App