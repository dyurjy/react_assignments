import React from 'react'


const Header = ( {title} ) => {
    return (
    <>
        <h1>
            {title}
        </h1>
    </>
    )
}

const Content = ( {parts} ) => {
    return (
        <>
            {parts.map( (val, i) =>
                <div key={i}>
                    <h3 key={i}>
                        Course Name: {val.name}
                    </h3>
                    <ul>
                        <li key={i}>
                            Number of Exercises: {val.exercises}
                        </li>
                    </ul>
                </div>
            )
            }
        </>
    )
            
}

const Summary = ( {parts} ) => {

    let exercisesArray = []

    parts.forEach( val => {
        exercisesArray.push(val.exercises)
    })

    return (
        <>
        <h4>
            Total Number of Exercises:{"\u00a0"}
            {exercisesArray.reduce( (prev, cur, i, array) => {
                return prev+cur
            }, 0)}  
        </h4>
        </>
    )
}

const Course = (props) => {
    return (
        <>
            {props.course.map( (val, i) => 
                <div key={i}>
                    <Header title={val.name} />
                    <Content  parts={val.parts} />
                    <Summary parts={val.parts} />
                </div>
            )}

        </>
    )
}

export default Course