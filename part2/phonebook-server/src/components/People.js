import React from 'react'

const People = (props) => {
    if (props.filter1.length > 0) {

        let subString = ""

        for (let i = 0; i < props.filter1.length; i++) {
            subString += props.filter1[i].toLowerCase()
        }

        let filterArray = props.numbers.filter( val => {
            if (val.name.substring(0, props.filter1.length).toLowerCase() == subString) {
                return val.name
            }
        })


        return (
            <ol>
                {filterArray.map((val, i) => 
                    <li key={i}>
                        {val.name} ----- {val.phone}
                    </li>
                )}
            </ol>
        )
    } else {
    } return (
        <>
            <ol>
                {props.numbers.map( (val, i) => 
                    <li key={i}>
                        {val.name} ----- {val.phone}
                    </li>
                )}
            </ol>
        </>
    )
}

export default People