const Weather = (props) => {

    if (Object.entries(props.weather).length === 0) {
        return (
            <>
            </>
        )
    } else {

        let style = {
                position: "absolute",
                display: "flex",
                maxWidth: "100px",
                minWidth: "100px",
                minHeight: "250px",
                maxHeight: "250px",
                flexDirection: "column",
                justifyContent: "center",
                left: "-104px",
                bottom: "-2px",
                backgroundColor: "lightblue",
                border: "2px solid",

            }


        let icon_url = `http://openweathermap.org/img/wn/${props.weather.weather[0].icon}@2x.png`

        return (
            <div style={style}>
                <h4 style={{textAlign: "center", margin: "2px 0px"}}>{props.weather.name} Weather</h4>
                <p style={{textAlign: "center", margin: "2px 0px"}}>
                    Temperature: {props.weather.main.temp}
                </p>
                <img src={icon_url} style={{maxWidth: "100px"}}>
                </img>
                <p style={{textAlign: "center", margin: "2px 0px"}}>
                    Wind Speed: {props.weather.wind.speed}
                </p>
            </div>
        )
    }
}

export default Weather