import Weather from "./Weather"

const Summary = (props) => {

    let style = {
        position: "absolute",
        top: 0,
        right: 0,
        minHeight: "100vh",
        maxHeight: "100vh",
        margin: "0px 20px 0px 0px",
        maxWidth: "300px",
        minWidth: "300px",
    }



    let fontSize = "28px"

    let headerStyle = {
        fontSize: fontSize,
        textAlign: "center",
        marginBottom: "0px"
    }

    let summCountry = props.countries.filter(val => {
        return val.name.official === props.selected
    })

    if (!(props.selected === "")) {


        if (summCountry[0].name.official.length > 30) {
            fontSize = "24px"
            headerStyle = {
                fontSize: fontSize,
                textAlign: "center",
                marginBottom: "0px"
            }
        }

        let languages = []
        Object.values(summCountry[0].languages).forEach(val => {
            if (typeof val === 'string') {
                languages.push(val)
            }
        })
        
        return (
            <div style={style}>
                <div style={{ maxHeight: "40%", minHeight: "inherit",display: "flex", flexDirection: "column", justifyContent: "space-around"}}>
                    <div style={{position: "relative", border: "2px solid", maxHeight: "400px", minHeight: "fit-content", paddingLeft: "0px", display: "flex", flexDirection: "column", justifyContent: "center"}}>
                        <h1 style={headerStyle}>
                            {summCountry[0].name.official}
                        </h1>
                        <h3 style={{textAlign: "center", marginBottom: "0px"}}>
                            Population: {summCountry[0].population.toLocaleString("en-us")}
                            <br/>
                            Capitol: {summCountry[0].capital}
                            <br/>
                            <br/>
                            Languages Spoken:
                        </h3>
                        {languages.map((val,i) => 
                            <p key={i} style={{textAlign: "center", marginTop: "2px", marginBottom: "0px"}}>
                                {val}
                            </p>
                        )}
                        <div>
                        <img style={{display: "block", margin: "auto", maxHeight: "140px", minWidth: "85%", maxWidth: "90%", border: "2px solid", marginBottom: "15px", marginTop: "10px"}} src={summCountry[0].flags.png} alt="The selected country's national flag."></img>
                        </div>
                        
                        <Weather capital={summCountry[0].capital} weather={props.weather}/>
                    </div>
                    
                </div>
            </div>
        )
    } else {
        return (
            <>
            </>
        )
    }
}

export default Summary