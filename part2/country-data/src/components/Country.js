const Header = (props) => {

    return (
        <>
            <li key={props.id} style={{margin: "3px 0px", maxWidth: "350px"}}>
                
                <button onClick={() => props.clicked(props.country.name.official)}>
                {props.country.name.official}
                </button>
            </li>
        </>
    )
}

export default Header