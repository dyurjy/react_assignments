import Country from './Country'

const Content = (props) => {

    let filtered = props.countries.filter( (val, i) => {
        return val.name.official.toLowerCase().includes(props.filter.toLowerCase())
        })

    if (filtered.length > 20) {
        return (
            <>
            </>
        )
    } else if (filtered.length>1) {
        return (
            <>
                <ol>
                    {filtered.map( (val, i) => 
                        <Country country={val} id={i} key={i} clicked={(arg) => props.clicked(arg)} />
                    )}
                </ol>
            </>
        )
    } else {
        return (
            <>
            </>
        )
    }
}

export default Content