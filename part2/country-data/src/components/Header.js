const Header = (props) => {

    return (
        <>
            <h3>
                Search for a country:{"  "}
                <input placeholder="search" onChange={(event) => props.change(event)} value={props.filter}></input>
            </h3> 
        </>
    )
}

export default Header