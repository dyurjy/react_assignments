import { useState, useEffect } from 'react'
import axios from 'axios'
import Header from './components/Header'
import Content from './components/Content'
import Summary from './components/Summary'

function App() {

  const [countries, setCountries] = useState([])
  const [filterValue, setFilterValue] = useState("")
  const [selected, setSelected] = useState("")
  const [weatherData, setWeatherData] = useState({})
  const [refresh, setRefresh] = useState(false)

  let fetchCountries = () => {
    axios
      .get('https://restcountries.com/v3.1/all')
      .then(response => {
        setCountries(response.data)
      })
  }
  

  let fetchWeather = () => {
    if (refresh === true) {
      let summCountry = countries.filter(val => {
        return val.name.official === selected
      })
      let weather_key = process.env.REACT_APP_WEATHER_KEY
    
      let cityName = summCountry[0].capital
    
      let fetchWeather = () => {
        axios
          .get(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=imperial&appid=${weather_key}`)
          .then(response => {
            setWeatherData(response.data)
          })
      }

      fetchWeather()

      setRefresh(false)
    }
  }

  useEffect(fetchCountries, [])
  useEffect(fetchWeather)

  let selectionHandler = (arg) => {
    setSelected(arg)
    setRefresh(true)
  }



  return (
    <div>
      <Header filter={filterValue} change={(event) => setFilterValue(event.target.value)} />
      <Content selected={selected} countries={countries} filter={filterValue} clicked={(arg) => selectionHandler(arg)}/>
      <Summary selected={selected} countries={countries} weather={weatherData} />
 </div>
  );
}

export default App;
