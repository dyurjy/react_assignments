import axios from 'axios'

const baseUrl = "/public/contacts"

const getAll = () => {
    return axios.get(baseUrl)
}

const create = newObject => {
    return axios.post(baseUrl, newObject) 
}

const update = (id, newObject) => {
    console.log(id)
    return axios.put(`${baseUrl}/${id}`, newObject)
}

const remove = (id) => {
    return axios.delete(`${baseUrl}/${id}`)
}

const funcs = {
    getAll,
    create,
    update,
    remove
}

export default funcs