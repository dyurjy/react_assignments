import React from 'react'

const Form = (props) => {

    return (
        <>
            <form onSubmit={props.onSubmit}>
                <div>
                    name:{"\u00a0"} <input value={props.newName} onChange={(event) => props.change(event)} />
                    <br/>
                    phone: <input value={props.newPhone} onChange={(event) => props.changePhone(event)} />
                </div>
                <div>
                    <button type="submit">add</button>
                </div>
            </form>
        </>
    )
}

export default Form