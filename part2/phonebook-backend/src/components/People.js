import React from 'react'

const People = (props) => {
    if (props.filter1.length > 0) {

        let subString = ""

        for (let i = 0; i < props.filter1.length; i++) {
            subString += props.filter1[i].toLowerCase()
        }

        let filterArray = props.numbers.filter( val => {
            return val.name.substring(0, props.filter1.length).toLowerCase() === subString
        })

        console.log(filterArray)


        return (
            <ol>
                {filterArray.map((val, i) => 
                    <li key={i} style={{margin: "5px 0px"}}>
                    {val.name} ----- {val.number} ----- 
                    <button style={{minHeight: "20px", maxHeight: "20px"}} onClick={() => props.clicked(val.id)}>Delete</button>
                </li>
                )}
            </ol>
        )
    } else {
    } return (
        <>
            <ol>
                {props.numbers.map( (val, i) => 
                    <li key={i} style={{margin: "5px 0px"}}>
                        {val.name} ----- {val.number} -----{"  "}
                        <button style={{minHeight: "20px", maxHeight: "20px"}} onClick={() => props.clicked(val.id)}>Delete</button>
                    </li>
                )}
            </ol>
        </>
    )
}

export default People