import React from 'react'

const Filter = (props) => {

    return (
        <div style={{display: "inline-block", marginLeft: "250px"}}>
            <h3 style={{textAlign: "center", margin: "0px"}}>
                Filter    
            </h3>
            <input style={{maxWidth: "80px"}} value={props.value} onChange={(event) => props.setFilter(event)}>
            </input>
        </div>
    )
}

export default Filter