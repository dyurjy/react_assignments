const ErrorMessage = (props) => {

    let divStyle = {
        display: "inline-block",
        position: "absolute",
        left: "275px",
        top: "75px",
        minWidth: "200px",
        maxWidth: "200px",
        border: "2px solid",
        borderRadius: "10px",
        minHeight: "60px",
        maxHeight: "60px",
        backgroundColor: "red",
    }

    let flexStyle = {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        minHeight: "inherit",
        minWidth: "inherit",
        maxHeight: "inherit",
        maxWidth: "inherit",
    }

    if (!(props.status == null)) {
        return (
            <div style={divStyle}> 
            <div style={flexStyle}>
            <h3 style={{margin: "0px", textAlign: "center"}}>
                Error
            </h3>
            <p style={{margin: "0px", textAlign: "center"}}>
                {props.status}
            </p>
            </div>
            </div>
        )
    } else {
        return (
            <>
            </>
        )
    }
}

export default ErrorMessage