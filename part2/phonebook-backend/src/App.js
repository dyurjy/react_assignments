import React, { useEffect, useState } from 'react'
import People from './components/People'
import Filter from './components/Filter'
import Form from './components/Form'
import ErrorMessage from './components/ErrorMessage'
import funcs from "./services/HttpPersons"
import OkMessage from './components/OkMessage'

const App = () => {
  const [persons, setPersons] = useState([])
  const [newName, setNewName] = useState('')
  const [newPhone, setNewPhone] = useState('')
  const [filterValue, setFilterValue] = useState('')
  const [errorMessage, setErrorMessage] = useState(null)
  const [okMessage, setOkMessage] = useState(null)

  let fetchPeople = () => {
    funcs.getAll()
      .then(response => {
        console.log(response.data)
        setPersons(response.data)
      })
      .catch(response => {
        errorMessageHandler(response.response.data)
      })
  }

  let createContact = (newObject) => {
    funcs.create(newObject)
      .then(response => {
        okMessageHandler("Contact has been added to phonebook.")
        fetchPeople()
      })
      .catch(response => {
        errorMessageHandler(response.response.data)
      })
  }

  let removeContact = (id) => {
    funcs.remove(id)
      .then(response => {
        okMessageHandler("Contact has been removed from the phonebook")
        fetchPeople()
      })
      .catch(response => {
        errorMessageHandler(response.response.data)
      })
  }

  let updateContact = (id, newObject) => {
    funcs.update(id, newObject)
      .then( (response) => {
        fetchPeople()
        okMessageHandler("Contact has been updated.")
      })
      .catch(response => {
        errorMessageHandler(response.response.data)
      })
  }

  useEffect(fetchPeople,[])

  let okMessageHandler = (message) => {
    setOkMessage(message)
    setTimeout( () => {
      setOkMessage(null)
    }, 4000)
  }

  let errorMessageHandler = (message) => {
    setErrorMessage(message)
    setTimeout( () => {
      setErrorMessage(null)
    }, 4000)
  }

  let formSubmitHandler = (e) => {
    e.preventDefault()

    let duplicateName = false
    let duplicatePhone = false
    let duplicateID = null

    persons.forEach( val => {
      if (val.name === newName) {
        duplicateName = true
        duplicateID = val.id
      }
    })
    persons.forEach( val => {
      if (val.phone === newPhone) {
        duplicatePhone = true
        duplicateID = val.id
      }
    })

    if (duplicateName && duplicatePhone) {
      errorMessageHandler("Name and number already in use.")
    } else if (duplicateName) {
      if (window.confirm(`Would you like to update the phone number for ${newName}?`)) {
        if (!(newPhone === "")) {
          updateContact(duplicateID, {name: newName, number: newPhone})
        } else {
          errorMessageHandler("Field(s) cannot be blank")
        }
      } else {
        okMessageHandler("Canceled update.")
      }

    } else if (!duplicateName && duplicatePhone) {
      errorMessageHandler("Phone number already in use")
    } else if (!duplicateName && !duplicatePhone) {
      if (!(newName === "") && !(newPhone === "")) {
        createContact({name: newName, number: newPhone})
        fetchPeople()
        setNewName("")
        setNewPhone("")
      } else if ((newName === "") || (newPhone === "")) {
          errorMessageHandler("Field(s) cannot be blank.")
      }
    }
  }

  //   if (!duplicateEntry && (!(newName === "")) && (!(newPhone === ""))) {
  //     let newObject = {
  //       name: newName,
  //       phone: newPhone
  //     }
  //     let newArray = persons.map( val => val)
  //     newArray.push(newObject)
  //     createContact(newName, newPhone)
  //     setPersons(newArray)
  //     setNewName("")
  //     setNewPhone("")
  //   } else if ((newName === "") || (newPhone === "" )) {
  //     alert("Field(s) cannot be blank")
  //   } else {
  //     setNewName("")
  //     setNewPhone("")
  //     alert(`name/phone is already listed in the phonebook.`)
  //   }
  // }

  let deleteButtonHandler = (arg) => {
    removeContact(arg)
  }

  return (
    <div>
      <h2 style={{display: "inline-block"}}>Phonebook</h2>
      <Filter setFilter={(event) => setFilterValue(event.target.value)} value={filterValue} />
      <ErrorMessage status={errorMessage} />
      <OkMessage status={okMessage} />
      <Form onSubmit={formSubmitHandler} newName={newName} change={(event) => setNewName(event.target.value)} newPhone={newPhone} changePhone={(event) => setNewPhone(event.target.value)} />
      <People numbers={persons} filter1={filterValue} clicked={(arg) => deleteButtonHandler(arg)} />
    </div>
  )
}

export default App