import React, { useState } from 'react'
import People from './components/People'
import Filter from './components/Filter'
import Form from './components/Form'

const App = () => {
  const [persons, setPersons] = useState([
    { name: 'Arto Hellas', phone: '040-123456', id: 1 },
    { name: 'Ada Lovelace', phone: '39-44-5323523', id: 2 },
    { name: 'Dylan Yurjevich', phone: '12-43-234345', id: 3 },
    { name: 'Mary Poppendieck', phone: '39-23-6423122', id: 4 }
  ])
  const [newName, setNewName] = useState('')
  const [newPhone, setNewPhone] = useState('')
  const [filterValue, setFilterValue] = useState('')

  let formSubmitHandler = (e) => {
    e.preventDefault()

    let duplicateEntry = false

    persons.forEach( val => {
      if (val.name === newName) {
        duplicateEntry = true
      }
    })
    persons.forEach( val => {
      if (val.phone === newPhone) {
        duplicateEntry = true
      }
    })

    if (!duplicateEntry && (!(newName === "")) && (!(newPhone === ""))) {
      let newObject = {
        name: newName,
        phone: newPhone
      }
      let newArray = persons.map( val => val)
      newArray.push(newObject)
      setPersons(newArray)
      setNewName("")
      setNewPhone("")
    } else if ((newName === "") || (newPhone === "" )) {
      alert("Field(s) cannot be blank")
    } else {
      setNewName("")
      setNewPhone("")
      alert(`name/phone is already listed in the phonebook.`)
    }
  }

  return (
    <div>
      <h2 style={{display: "inline-block"}}>Phonebook</h2>
      <Filter setFilter={(event) => setFilterValue(event.target.value)} name={newName} />
      <Form onSubmit={formSubmitHandler} newName={newName} change={(event) => setNewName(event.target.value)} newPhone={newPhone} changePhone={(event) => setNewPhone(event.target.value)} />
      {/* <People numbers={persons} filter1={filterValue} /> */}
    </div>
  )
}

export default App