import React from 'react'

const Filter = (props) => {

    return (
        <div style={{display: "inline-block", marginLeft: "250px"}}>
            <input style={{maxWidth: "80px"}} value={props.name} onChange={(event) => props.setFilter(event)}>
            </input>
        </div>
    )
}

export default Filter